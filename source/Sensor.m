classdef Sensor < handle
    % Sensor High-level class to work with sensors.
    %
    % Properties:
    %   Standard
    %       debug           - Debug mode turned on or off
    %       mode            - Sensor mode in which the value will be read
    %   Dependent
    %       value           - Value read from sensor
    %   get-only
    %       isConnected     - Is virtual brick connected to physical one?
    %       brick           - Brick object from sublayer class Brick -> used as interface to the physical brick.
    %
    % Methods:
    %   Standard
    %       EV3             -
    %       connect         - Connects Sensor-object to physical brick.
    %       disconnect      - Disconnects Sensor-object from physical brick.
    %       update          - Updates Sensor-object to current status at its port.
    %       reset           - Resets value on sensor
    %       setProperties   - Sets multiple Sensor properties at once using MATLAB's inputParser.
    %
    % Example
    %  % This small example should only roughly demonstrate how to work with sensors.
    %  % Establish connection, set properties on sensor1 and wait, until user has pressed the 
    %  % touch sensor ten times. Then beep and disconnect.
    %
    %  b = EV3();
    %  b = EV3('debug', 'on', 'batteryMode', 'Voltage');
    %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
    %  b.sensor1.mode = DeviceMode.Touch.Bumps;
    %  while value < 10
    %     pause(0.1);
    %  end
    %  b.beep();
    %  b.disconnect();
    %  b.delete()
    %
    %
    % Signature
    %  Author: Tim Stadtmann
    %  Date: 2016/05/20
    
    properties  % Standard properties to be set by user
        %debug -  Debug turned on or off
        % In debug mode, everytime a command is passed to the sublayer ('communication layer'),
        % there is feedback in the console about what command has been called, etc.
        %   -> Any valid boolean (0/1/'on'/'off'/'true'/'false')
        debug;
        
        %mode -  Sensor mode in which the value will be read
        %   -> DeviceMode.[...] (except for DeviceMode.Motor.[...])
        mode;
    end
    
    properties (Dependent)  % Parameters to be read directly from physical brick
        value  % Value read from sensor
    end

    properties (SetAccess = 'private')  % Read-only properties that are set internally or in init-phase
        %sensorAtPort -  Is physical sensor actually connected to port?
        % This property will be changed by Sensor.update() if changes have happened on the
        % physical brick, depending on status, type and mode values.
        % See also STATUS, TYPE, MODE, UPDATE
        sensorAtPort = 0;
        
        %brick -  Brick object from sublayer class Brick -> used as interface to the physical brick.
        % See also BRICK
        brick = 0;
        
        %port -  Sensor port
        % This is only the string representation of the sensor port to work with.
        % Internally, SensorPort-enums are used. (This is a bit inconvenient but more
        % consistent compared with Motor)
        %   -> '1' / '2' / '3' / '4'
        port;
        
        %status -  Connection status at port
        % This property will be changed by Sensor.update() if changes have happened on the
        % physical brick.
        % See also CONNECTIONTYPE, UPDATE
        status = DeviceMode.Error.Undefined;  
        
        %type -  Device type at port
        % This property will be changed by Sensor.update() if changes have happened on the
        % physical brick.
        % See also DEVICEMODE.ERROR, UPDATE
        type = DeviceMode.Error.Undefined; 
    end   
    
    properties (Hidden, Access = 'private')  % Hidden properties for internal use only
        init = 1;  % Is set to zero after initial 'creating' phase is done.
        isConnected = 0;  % Does (virtual) sensor-object have a valid brick-handle?
        
        %% Equivalent for Sensor.port in enumeration SensorPort
        % Port is an actual parameter on the physical brick. To avoid using string
        % comparisons each time it is used, the corresponding value (i.e. '1' -> SensorPort.Sensor1)
        % to the given strings is saved (hidden from the user).
        port_;
    end
    
    methods  % Standard methods
        %% Constructor
        function sensor = Sensor(varargin)
            sensor.setProperties(varargin{1:end});
			sensor.init = 0;
        end
        
        %% Connection
        function connect(sensor,brick)
            %connect Connects Sensor-object to physical brick
            if sensor.isConnected
                if isBrickValid(sensor.brick)
                    error('Sensor::connect: Sensor-Object already has a brick handle.');
                else
                    warning(['Sensor::connect: Sensor.isConnected is set to ''True'', but ',...
                             'brick handle is invalid. Deleting invalid handle and ' ,...
                             'resetting Sensor.isConnected now...']);
                         
                    sensor.brick = 0;
                    sensor.isConnected = 0;
                    
                    error('Sensor::connect: Aborted connection.');
                end
            end
            
            sensor.brick = brick;
            sensor.isConnected = 1;
            
            if sensor.debug
                fprintf('(DEBUG) Sensor-Object connected to brick handle.\n');
            end
            
            sensor.update();
        end
        
        function disconnect(sensor)
            %disconnect Disconnects Sensor-object from physical brick
            if ~sensor.isConnected
                error('Sensor::disconnect: No brick connected.');
            end
            
            sensor.brick = 0; % Note: actual deleting is done in EV3::disconnect.
            sensor.isConnected = 0;
            sensor.sensorAtPort = 0;
            
            sensor.status = DeviceMode.Error.Undefined;
            sensor.type = DeviceMode.Error.Undefined;
        end
        
        function update(sensor)
            %update Updates Sensor-object to current status at its port.
            if ~sensor.isConnected
                error(['Sensor::update: Sensor-Object not connected to brick handle.',...
                    'You have to call sensor.connect(brick) first!']);
            end
            
            oldMode = sensor.mode;
                       
            sensor.status = sensor.getStatus();
            [sensor.type, newMode] = sensor.getTypeMode();
            
            if strcmp(class(oldMode),class(newMode)) && oldMode~=newMode
                if ~strcmp(class(oldMode), 'DeviceMode.Error') && ...
                        ~strcmp(class(newMode), 'DeviceMode.Error')
                    warning(['Sensor::update: Physical sensor''s mode was not ',...
                        'the specified one. Changing...']);
                    
                    sensor.setMode(oldMode);
                    sensor.mode = oldMode;
                end
            else 
                sensor.mode = newMode;
            end
			
            validStatus = [ConnectionType.NXTColor, ConnectionType.NXTDumb, ...
                           ConnectionType.InputDumb, ConnectionType.InputUART];
            validTypes = [DeviceType.NXTTouch, DeviceType.NXTColor, ...
                            DeviceType.NXTLight, DeviceType.NXTSound, ...
                            DeviceType.NXTTemperature, DeviceType.NXTUltraSonic, ...
                            DeviceType.Color, DeviceType.Gyro, DeviceType.InfraRed, ...
                            DeviceType.Touch, DeviceType.UltraSonic];
            
            sensor.sensorAtPort = 1;
            if ~ismember(sensor.status, validStatus) || ~ismember(sensor.type, validTypes) || ...
                    strcmp(class(sensor.mode), 'DeviceMode.Error')
               sensor.sensorAtPort = 0;
            end
        end
        
        %% Brick functions
        function reset(sensor)
            %reset Resets value on sensor
			% Note: This clears ALL the sensors right now, no other Op-Code available... :(
            if ~sensor.isConnected
                error(['Sensor::reset: Sensor-Object not connected to brick handle.',...
                       'You have to call sensor.connect(brick) first!']);
            elseif ~sensor.sensorAtPort
                error('Sensor::reset: No physical sensor connected to Port %s.',...
                       sensor.port);
            end
            
            warning(['Sensor::reset: Current version of reset resets ALL devices, that is, ',...
                     'all motor tacho counts and all other sensor counters!']);
            sensor.brick.inputDeviceClrAll(0);
            
            if sensor.debug
                fprintf('(DEBUG) Sensor::reset: Called inputReadSI on Port %s.\n',...
                    sensor.port);
            end
        end
        
        %% Setter
        function set.brick(sensor, brick)
            if ~isBrickValid(brick)
                error('Sensor::set.brick: Handle to brick not valid.');
            else
                sensor.brick = brick;
            end
        end
        
        function set.port(sensor, port)
            if ~isPortValid(class(sensor),port)
                error('Sensor::set.port: Given port is not a valid port.');
            end
            
            if ischar(port)
                sensor.port = port;
                sensor.port_ = str2PortParam(class(sensor), port);
            else
                error('Sensor::set.port: Port has to be a string.');
            end
        end

        function set.debug(sensor, debug)
            % Check if debug is valid and set sensor.debug if it is.
            if ~isBool(debug)
                error('Sensor::set.debug: Given parameter is not a bool.');
            end
            
            if ischar(debug)
                sensor.debug = str2bool(debug);
            else
                sensor.debug = debug;
            end
        end
		
        function set.mode(sensor, mode)
            if strcmp(class(mode),'DeviceMode.Error') && ~sensor.sensorAtPort
                sensor.mode = mode;
                return;
            end
            
            if strcmp(class(mode), 'uint8') || strcmp(class(mode), 'double')
                mode = DeviceMode(sensor.type, uint8(mode));
            end
            
            if ~isModeValid(mode, sensor.type)
                error('Sensor::set.mode: Sensor mode is not valid.');
            else
                sensor.mode = mode;
                sensor.setMode(mode);
            end
        end
        
        function setProperties(sensor, varargin)
            %setProperties Sets multiple Sensor properties at once using MATLAB's inputParser.
            %
            % Arguments
            %  * 'debug', 0/1/'on'/'off'/'true'/'false'
            %  * 'mode', DeviceMode.[...]
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b.sensor1.setProperties('debug', 'on', 'mode', DeviceMode.Color.Ambient);
            %  % Instead of: b.sensor1.debug = 'on'; 
            %  %             b.sensor1.mode = DeviceMode.Color.Ambient;
            %
            
            p = inputParser();
            
            % Set default values
            if sensor.init
                defaultDebug = 0;
                defaultMode = DeviceMode.Error.Undefined;
            else
                defaultDebug = sensor.debug;
                defaultMode = sensor.mode;
            end
            
            % Add parameter
            if sensor.init
                p.addRequired('port');
            end
            p.addOptional('debug', defaultDebug);
            p.addOptional('mode', defaultMode);
            
            % Parse...
            p.parse(varargin{:});
            
            % Set properties
            if sensor.init
                sensor.port = p.Results.port;
            end
            sensor.mode = p.Results.mode;
            sensor.debug = p.Results.debug;
        end
        
        %% Getter
        function value = get.value(sensor)
            if ~sensor.isConnected || ~sensor.sensorAtPort
                warning('Sensor::get.value: Could not detect sensor at port %s.', ...
                         sensor.port);
                
                value = 0;
                return;
            end
            
            value = sensor.getValue();
        end
        
        %% Display
        function display(sensor)
            warning('off','all');
            builtin('disp', sensor);
            warning('on','all');
        end
    end
    
    methods (Access = 'private')  % Private brick functions that are wrapped by dependent params
        function setMode(sensor, mode)
            %             if ~sensor.isConnected
            %                 error(['Sensor::setMode: Sensor-Object not connected to brick handle.',...
            %                        'You have to call sensor.connect(brick) first!']);
            %             end
            if ~sensor.isConnected || ~sensor.sensorAtPort
                return
            end
            
            sensor.brick.inputReadSI(0, sensor.port_, mode);  % Reading a value implicitly
                                                              % sets the mode.
            
            if sensor.debug
                fprintf('(DEBUG) Sensor::setMode: Called inputReadSI on Port %s\n',...
                    sensor.port);
            end
        end
        
        function val = getValue(sensor)
            %getValue Reads value from sensor
            % 
            % Notes
            %  * After changing the mode, sensors initially always send an invalid value. In
            %    this case, the inputReadSI-opCode is sent again to get the correct value.
            %
            
            if ~sensor.isConnected
                error(['Sensor::getValue: Sensor-Object not connected to brick handle.',...
                       'You have to call sensor.connect(brick) first!']);
            elseif ~sensor.sensorAtPort
                error('Sensor::getValue: No physical sensor connected to Port %d.',...
                       sensor.port);
            end
            
            val = sensor.brick.inputReadSI(0, sensor.port_, sensor.mode);
            
            if strcmp(class(sensor.mode), 'DeviceMode.Color')
                if sensor.mode == DeviceMode.Color.Col
                    val = Color(val);
                end
            end
            
			if isnan(val)
				pause(0.01);
				val = sensor.brick.inputReadSI(0, sensor.port_, sensor.mode);
				if isnan(val)
					warning('Sensor::getValue: Brick returned invalid value (NaN). Try again...');
				end
			end
			
            if sensor.debug
                fprintf('(DEBUG) Sensor::getValue: Called inputReadSI on Port %d.\n',...
                    sensor.port);
            end
        end
        
        function status = getStatus(sensor)
           if ~sensor.isConnected
                error(['Sensor::getStatus: Sensor-Object not connected to brick handle.',...
                       'You have to call sensor.connect(brick) first!']);
           end
           
            statusNo = sensor.brick.inputDeviceGetConnection(0, sensor.port_);
            status = ConnectionType(statusNo);
            
            if sensor.debug
                fprintf('(DEBUG) Sensor::getStatus: Called inputDeviceGetConnection on Port %s\n',...
                         sensor.port);
            end
        end
        
        function [type,mode] = getTypeMode(sensor)
           if ~sensor.isConnected
                error(['Sensor::getTypeMode: Sensor-Object not connected to brick handle.',...
                       'You have to call sensor.connect(brick) first!']);
           end
           
            [typeNo,modeNo] = sensor.brick.inputDeviceGetTypeMode(0, sensor.port_);
            type = DeviceType(typeNo);
            mode = DeviceMode(type,modeNo);
            
            if sensor.debug
                fprintf('(DEBUG) Sensor::getTypeMode: Called inputDeviceGetConnection on Port %s\n',...
                         sensor.port);
            end
        end
    end
end

