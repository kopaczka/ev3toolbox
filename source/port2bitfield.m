function bitfield = port2bitfield(varargin)
% Converts a motorPort-enum to motorBitfield-enum.
    if nargin==1
        bitfield = MotorBitfield(2 ^ varargin{1});
    elseif nargin==2
        bitfield = MotorBitfield(2 ^ (varargin{1}+varargin{2}));
    end
end
