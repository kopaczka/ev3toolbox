function isValid = isSensorValid(sensor)
% Returns whether given sensor object is valid or not.
    isValid = 0;
    if ~isempty(sensor)
        if isa(sensor, 'Sensor') && sensor.isvalid
            isValid = 1;
        end
    end
end
