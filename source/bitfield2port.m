function port = bitfield2port(bitfield)
% Converts motorBitfield-enum to motorPort-enum.
	port = MotorPort(log2(double(bitfield)));
end