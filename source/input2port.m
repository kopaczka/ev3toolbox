function port = input2port(input)
% Converts motorInput-enum to motorPort-enum.
    port = MotorPort(input-16);
end