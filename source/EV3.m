classdef EV3 < handle
    % High-level class to work with physical bricks.
    %
    % This is the 'central' class (from user's view) when working with this toolbox. It
    % delivers a convenient interface for creating a connection to the brick and sending
    % commands to it. 
    % 
    % Properties:
    %   Standard
    %       motorA[,B,C,D]  - 
    %       sensor1[,2,3,4] -
    %       debug           - 
    %       batteryMode     - Mode for reading battery charge
    %   Dependent
    %       batteryValue    - Current battery charge level
    %   get-only
    %       isConnected     - Is virtual brick connected to physical one?
    %       brick           - Brick object from sublayer class Brick -> used as interface to the physical brick.
    %
    % Methods:
    %   Standard
    %       EV3             -
    %       connect         - Connects EV3-object and its Motors and Sensors to physical brick via bt or usb.
    %       disconnect      - Disconnects EV3-object and its Motors and Sensors from physical brick.
    %       update          - Updates all Motors and Sensors
    %       coupleMotors    - Creates and connects a SyncMotor-object using two chosen Motor-objects 
    %       beep            - Plays a 'beep' tone on brick.
    %       playTone        - Plays tone on brick.
    %       stopTone        - Stops tone currently played.
    %       tonePlayed      - Tests if a tone is currently played.
    %       setProperties   - Set multiple EV3 properties at once using MATLAB's inputParser.
    %
    %
    % Notes
    %  * Creating multiple EV3 objects and connecting them to different physical bricks has not
    %    been thoroughly tested yet, but seems to work on a first glance.
    %  * When referring to an instance of this class the term 'virtual brick' is used from now
    %    on. The LEGO brick itself is referred to as 'physical brick'.
    %
    % Example
    %  % This small example connects to the brick, starts motorA with power 50, reads a value
    %  % from sensor1, brakes the motor and disconnects again.
    %  % This should roughly demonstrate how this toolbox works.
    % 
    %  brickObject = EV3();
    %  brickObject = EV3('debug', 'on', 'batteryMode', 'Voltage');
    %  brickObject.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
    %  m = brickObject.motorA;
    %  m.power = 50;
    %  m.start();
    %  s = brickObject.sensor1;
    %  s.value
    %  m.tachoCount
    %  m.brakeMode = 'Brake';
    %  m.stop();
    %  brickObject.disconnect();
    %  delete brickObject;
    %  delete m; delete s;
    %
    %
    % Signature
    %  Author: Tim Stadtmann
    %  Date: 2016/05/19
    
    properties  % Standard properties 
        %% Modi
        
        %batteryMode -  Mode for reading battery charge
        %   -> 'Percentage' / 'Voltage'
        % See also EV3.BATTERYVALUE
        batteryMode = 'Percentage';
        
        %% Debug
        
        debug;
    end

    properties (Dependent)  % Parameters to be read directly from physical brick
        batteryValue;  % Current battery status (either in Percentage or Voltage)
    end
    
    properties (SetAccess = 'private')  % Read-only properties that are set internally
        isConnected = 0;  % Is virtual brick currently connected to physical brick?
        brick = 0;  % Brick object from sub-layer class Brick -> used as interface to the 
                    % physical brick.
                    
        %% Motors and Sensors
        
        motorA;
        motorB;
        motorC;
        motorD;
        
        sensor1;
        sensor2;
        sensor3;
        sensor4;            
    end
    
    properties (Hidden, Access = 'private')  % Hidden properties for internal use only
        init = 1;  % Indicates 'init-phase' (Set to 1 as long as constructor is running)
        verbose = 0;  % Debug mode for low level layers
    end
    
    methods  % Standard methods
        %% Constructor
        function ev3 = EV3(varargin)
            % Sets properties of EV3-object and creates Motor- and Sensor-objects with default
            % parameters.
            %
            % Arguments
            %  * varargin: see EV3::setProperties(ev3, varargin)
            %
            
            ev3.setProperties(varargin{:});
            
            ev3.motorA = Motor('A', 'Debug', ev3.debug-ev3.verbose);
            ev3.motorB = Motor('B', 'Debug', ev3.debug-ev3.verbose);
            ev3.motorC = Motor('C', 'Debug', ev3.debug-ev3.verbose);
            ev3.motorD = Motor('D', 'Debug', ev3.debug-ev3.verbose);
            
            
            ev3.sensor1 = Sensor('1', 'Debug', ev3.debug-ev3.verbose);
            ev3.sensor2 = Sensor('2', 'Debug', ev3.debug-ev3.verbose);
            ev3.sensor3 = Sensor('3', 'Debug', ev3.debug-ev3.verbose);
            ev3.sensor4 = Sensor('4', 'Debug', ev3.debug-ev3.verbose);
            
            ev3.init = 0;
        end
        
        %% Connection 
        function connect(ev3, varargin)
            %connect Connects EV3-object and its Motors and Sensors to physical brick.
            %
            % Arguments
            %  * 'beep', 'on'/'off': EV3 beeps if connection has been established.
            %  * 'ioType', 'bt'/'usb': Connection type
            %  * ['ioType', 'bt'] 'serPort', '/dev/rfcommx': Path to serial port
            %
            % Examples
            %  b = EV3(); b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b = EV3(); b.connect('beep', 'on', ''ioType', 'usb'); 
            %
            
            % Check connection
            if ev3.isConnected
                if isBrickValid(ev3.brick)
                    error('EV3::connect: Already connected.');
                else
                    warning(['EV3::connect: EV3.isConnected is set to ''True'', but ',...
                             'brick handle is invalid. Deleting invalid handle and ' ,...
                             'resetting EV3.isConnected now...']);
                         
                    ev3.brick = 0;
                    ev3.isConnected = 0;
                    
                    % error('EV3::connect: Aborted connection.');
                end
            end
            
            % Interpret arguments
            if nargin < 3
                 error('EV3::connect: Wrong number of input arguments.');
            end
            beep = 0;
            if strcmpi(varargin{1}, 'beep')
                if strcmpi(varargin{2}, 'on')
                    beep = 1;
                elseif ~strcmpi(varargin{2}, 'off')
                    % First argument was beep, but second one was neither 'true' nor 
                    % 'false'
                    warning(['EV3::connect: The parameter after "beep" has to be either ',... 
                             '"true" or "false"']);
                    error('EV3::connect: Wrong input arguments.');
                end
                varargin = varargin(3:end);  % Cut beep-part off
            end
            
            % Try to connect
            try 
                % Connect to physical brick
                ev3.brick = Brick('debug', ev3.verbose, varargin{1:end});  % Creating Brick-object implicitly 
                                                                           % establishes connection
                ev3.isConnected = 1;
                
                if beep
                    ev3.beep();
                end
                
                % Connect motors
                ev3.motorA.connect(ev3.brick);
                ev3.motorB.connect(ev3.brick);
                ev3.motorC.connect(ev3.brick);
                ev3.motorD.connect(ev3.brick);
                
                % Connect sensors
                ev3.sensor1.connect(ev3.brick);
                ev3.sensor2.connect(ev3.brick);
                ev3.sensor3.connect(ev3.brick);
                ev3.sensor4.connect(ev3.brick);
            catch ME
                % Something went wrong...
                ev3.isConnected = 0;
                if isBrickValid(ev3.brick) && ev3.brick ~= 0
                    ev3.brick.delete();
                    ev3.brick = 0;
                end
                error(['Brick::connect: Something went wrong, try again...\n', ...
                       'Error identifier: %s'], ME.identifier);
            end
        end
        
        function disconnect(ev3)
            %disconnect Disconnects EV3-object and its Motors and Sensors from physical brick.
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  % do stuff
            %  b.disconnect();
            %
            
            if ~ev3.isConnected
                error('EV3::connect: No brick connected.');
            end
            
            % Disconnect motors
            % -> set references to brick object to 0
            ev3.motorA.disconnect();
            ev3.motorB.disconnect();
            ev3.motorC.disconnect();
            ev3.motorD.disconnect();
            
            % Disconnect sensors
            % -> set references to brick object to 0
            ev3.sensor1.disconnect();
            ev3.sensor2.disconnect();
            ev3.sensor3.disconnect();
            ev3.sensor4.disconnect();
            
            % Delete handle to brick object
            ev3.brick.delete();
            ev3.brick = 0;
            
            ev3.isConnected = 0;
        end
        
        function update(ev3)
            %update Updates all Motors and Sensors to current status of their corresponding ports.
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  % Connect some new motors and sensors, disconnect some others (in 'real life')
            %  b.update();
            %  % Now, virtual brick knows about all new devices and commands can be sent to
            %  % them
            %
            
            if ~ev3.isConnected
                error('EV3::update: No brick connected.');
            end    
            
            ev3.motorA.update();
            ev3.motorB.update();
            ev3.motorC.update();
            ev3.motorD.update();
            
            ev3.sensor1.update();
            ev3.sensor2.update();
            ev3.sensor3.update();
            ev3.sensor4.update();
        end
        
        %% System functions
        function syncMotor = coupleMotors(ev3, varargin)
            %coupleMotors Creates and connects a SyncMotor-object using two chosen Motor-objects. 
            %
            % Notes
            %  * Right now only possible if connection to Brick has been established.
            %
            % Arguments
            %  * port ('AB','BA', ...): Motors to 'couple'
            %  * 'useMotorParams', 0/1/'on'/'off'/'true'/'false': Use first motor's or set own params
            %  * varargin: other Properties for Motor/SyncMotor (see
            %                                       (Sync-)Motor::setProperties(motor, varargin))
            %
            % Example
            %  b = EV3(); b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  sync = b.coupleMotors('AB'); % Couple motors A and B using parameters of
            %                               % Motor-object motorA.
            %  sync2 = b.coupleMotors('CD', 'useMotorParams', 0, 'power', 50, 'debug', 'on');
            %                               % Couple motors C and D with new object being 
            %                               % initialized with default resp. given parameters.
            %
            
            if ~ev3.isConnected
                error('EV3::update: No brick connected.');
            end    
            
            % Check input arguments
            p = inputParser();
            p.KeepUnmatched = 1;
            p.addRequired('port', @(x) isPortValid('SyncMotor', x));
            p.addOptional('useMotorParams', 1, @(x) isBool(x));
            p.parse(varargin{:});
            
            useMotorParams = str2bool(p.Results.useMotorParams);
            if strcmpi(p.Results.port, 'AB') 
                motor1 = ev3.motorA;
                motor2 = ev3.motorB;
            elseif strcmpi(p.Results.port, 'BA')
                motor1 = ev3.motorB;
                motor2 = ev3.motorA;
            elseif strcmpi(p.Results.port, 'AC') 
                motor1 = ev3.motorA;
                motor2 = ev3.motorC;               
            elseif strcmpi(p.Results.port, 'CA')
                motor1 = ev3.motorC;
                motor2 = ev3.motorA;              
            elseif strcmpi(p.Results.port, 'AD')
                motor1 = ev3.motorA;
                motor2 = ev3.motorD;              
            elseif strcmpi(p.Results.port, 'DA')
                motor1 = ev3.motorD;
                motor2 = ev3.motorA; 
            elseif strcmpi(p.Results.port, 'BC')
                motor1 = ev3.motorB;
                motor2 = ev3.motorC;
            elseif strcmpi(p.Results.port, 'CB')
                motor1 = ev3.motorC;
                motor2 = ev3.motorB;
            elseif strcmpi(p.Results.port, 'BD')
                motor1 = ev3.motorB;
                motor2 = ev3.motorD;
            elseif strcmpi(p.Results.port, 'DB')
                motor1 = ev3.motorD;
                motor2 = ev3.motorB;
            elseif strcmpi(p.Results.port, 'CD')
                motor1 = ev3.motorC;
                motor2 = ev3.motorD;
            elseif strcmpi(p.Results.port, 'DC')
                motor1 = ev3.motorD;
                motor2 = ev3.motorC;
            end
            
            % Create SyncMotor-object with parameters of motor1 and given parameters
            if useMotorParams
                syncMotor = SyncMotor(motor1, motor2, p.Results.port, ...
                                      'Power', motor1.power, ...
                                      'SpeedRegulation', motor1.speedRegulation, ...
                                      'BrakeMode', motor1.brakeMode, ...
                                      'LimitMode', motor1.limitMode, ...
                                      'LimitValue', motor1.limitValue, ...
                                      'SmoothStart', motor1.smoothStart, ...
                                      'SmoothStop', motor1.smoothStop, ...
                                      'Debug', motor1.debug);
            else
                syncMotor = SyncMotor(motor1, motor2, p.Results.port, varargin{4:end});
            end
            
            % Connect SyncMotor-object to brick handle.
            syncMotor.connect(ev3.brick);
        end
        
        %% Sound functions
        function beep(ev3)
            %beep Plays a 'beep' tone on brick.
            %
            % Notes
            %  * This equals playTone(10, 1000, 100). (Wraps the same opCode in comm-layer)
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b.beep(); 
            %
            
            if ~ev3.isConnected
                error(['EV3::beep: Brick-Object not connected physical brick. ',...
                       'You have to call ev3.connect(...) first!']);
            end
            
            ev3.brick.beep();
            
            if ev3.debug
				fprintf('(DEBUG) EV3::beep: Called beep on brick\n');
            end
        end
        
        function playTone(ev3, volume, frequency, duration)
            %playTone Plays tone on brick.
            %
            % Arguments
            %  * volume (0...100)
            %  * frequency (250...10000)
            %  * duration (in milliseconds)
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b.playTone(50, 5000, 1000);  % Plays tone with 50% volume and 5000Hz for 1
            %                               % second.
            %
            
            if ~ev3.isConnected
                error(['EV3::isConnected: Brick-Object not connected physical brick. ',...
                       'You have to call ev3.connect(...) first!']);
            end
            
            ev3.brick.soundPlayTone(volume, frequency, duration);
            
            if ev3.debug
				fprintf('(DEBUG) EV3::beep: Called soundPlayTone on brick\n');
            end
        end
        
        function stopTone(ev3)
            %stopTone Stops tone currently played.
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b.playTone(10,100,100000000);  % Accidentally given wrong tone duration.
            %  b.stopTone();  % Stops tone immediately.
            %
            
            if ~ev3.isConnected
                error(['EV3::stopTone: Brick-Object not connected physical brick. ',...
                       'You have to call ev3.connect(...) first!']);
            end
            
            ev3.brick.soundStopTone();
            
            if ev3.debug
				fprintf('(DEBUG) EV3::beep: Called soundStopTone on brick\n');
            end
        end
        
        function status = tonePlayed(ev3)
            %tonePlayed Tests if tone is currently played.
            %
            % Output
            %  * status: Returns 0 if a tone is being played, 1 if not.
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b.playTone(10, 100, 1000);
            %  pause(0.5);
            %  b.tonePlayed() -> Outputs 1 to console.
            %
            
            if ~ev3.isConnected
                error(['EV3::tonePlayed: Brick-Object not connected physical brick. ',...
                       'You have to call ev3.connect(...) first!']);
            end
            
            status = ev3.brick.soundTest;
            
            if ev3.debug
				fprintf('(DEBUG) EV3::beep: Called soundTest on brick\n');
            end
        end
        
        %% Setter
        function set.brick(ev3, brick)
            % Check if brick is valid and set ev3.brick if it is.
            
            if ~isBrickValid(brick)
                error('EV3::set.brick: Handle to Brick-object not valid.');
            else
                ev3.brick = brick;
            end
        end
        
        function set.batteryMode(ev3, batteryMode)
            % Check if batteryMode is valid and set ev3.batteryMode if it is.
            %
            % Arguments
            %  * batteryMode ('Voltage'/'Percentage'): Mode in which batteryValue will be read.
            %
            
            validModes = {'Voltage', 'Percentage'};
            if ~ischar(batteryMode) || ~any(validatestring(batteryMode, validModes))
                error('EV3::set.batteryMode: Given parameter is not a valid battery mode.');
            else 
                ev3.batteryMode = batteryMode;
            end
        end
        
        function set.debug(ev3, debug)
            % Check if debug is valid and set ev3.debug if it is. 
            %
            % Arguments
            %  * debug (0/1/'on'/'off'/'true'/'false'/2)
            %
            
            if ~isBool(debug) && debug ~= 2
                error('EV3::set.debug: Given parameter is not a bool.');
            end
            
            if ischar(debug)
                ev3.debug = str2bool(debug);
            else
                ev3.debug = debug;
            end
            
            if debug == 2
                ev3.verbose = 1;
            end
        end
        
        function setProperties(ev3, varargin)
            %setProperties Set multiple EV3 properties at once using MATLAB's inputParser.
            %
            % Arguments
            %  * 'debug', 0/1/'on'/'off'/'true'/'false'
            %  * 'batteryMode', 'Voltage'/'Percentage': Mode in which batteryValue will be read.
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b.setProperties('debug', 'on', 'batteryMode', 'Voltage');
            %  % Instead of: b.debug = 'on'; b.batteryMode = 'Voltage';
            %
            
            p = inputParser();
            
            % Set default values
            if ev3.init
                defaultDebug = 0;
                defaultBatteryMode = 'Percentage';
            else
                defaultDebug = ev3.debug;
                defaultBatteryMode = ev3.batteryMode;
            end
            
            % Add parameter
            p.addOptional('debug', defaultDebug);
            p.addOptional('batteryMode', defaultBatteryMode);
            
            % Parse...
            p.parse(varargin{:});
            
            % Set properties
            ev3.batteryMode = p.Results.batteryMode;
            ev3.debug = p.Results.debug;
        end
        
        %% Getter
        function bat = get.batteryValue(ev3)
            if ~ev3.isConnected
                warning('EV3::getBattery: EV3-Object not connected to physical EV3.');
                
                bat = 0;
                return;
            end
            
            bat = ev3.getBattery();
        end
        
        %% Display
        %% Display
        function display(ev3)
            % WIP
            warning('off','all');
            builtin('disp', ev3);
            warning('on','all');
        end
    end
    
    methods (Access = 'private')  % Private brick functions that are wrapped by dependent params
        function bat = getBattery(ev3)
            if ~ev3.isConnected
               error(['EV3::getBattery: EV3-Object not connected to physical EV3. You have ',... 
                      'to call ev3.connect(properties) first!']);
            end
            
            if strcmpi(ev3.batteryMode, 'Percentage')
                bat = ev3.brick.uiReadLbatt();
                
                if ev3.debug
                    fprintf('(DEBUG) EV3::getBattery: Called uiReadLBatt.\n');
                end
            elseif strcmpi(ev3.batteryMode, 'Voltage')
                bat = ev3.brick.uiReadVbatt();
                
                if ev3.debug
                    fprintf('(DEBUG) EV3::getBattery: Called uiReadVBatt.\n');
                end
            else
                warning('EV3:getBattery: Mode has to be Percentage or Voltage!');
                error('EV3::getBattery: Mode not valid.');
            end
        end
    end
end 
