function isValid = isPortValid(device, port)
% Returns whether given port-string is valid or not.
    isValid = 0;
    
    if ~ischar(device)
        error(['isPortValid: First argument has to be a string (''Sensor'', ',...
            '''Motor'' or ''SyncMotor'')']);
    end
    
    if strcmpi(device, 'Motor')
        validPorts = {'A', 'B', 'C', 'D'};
    elseif strcmpi(device, 'Sensor')
        validPorts = {'1', '2', '3', '4'};
    elseif strcmpi(device, 'SyncMotor')
        validPorts = {'AB', 'AC', 'AD', 'BA', 'BC', 'BD', ...
            'CA', 'CB', 'CD', 'DA', 'DB', 'DC'};
    else
        error(['isPortValid: First argument has to be either ''Sensor'', ',...
               '''Motor'' or ''SyncMotor''']);
    end
    
    % Note: Both port2str() and validatestring() generate errors if the input
    %       argument is 'wrong'. This method here isn't supposed to do this,
    %       it's only supposed to return whether given port is valid or not.
    try
        if ~ischar(port)
            port = port2str(device, port);
        end

        if any(validatestring(port, validPorts))
            isValid = 1;
        end
    catch
        return;
    end
end
