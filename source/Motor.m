classdef Motor < handle 
    % Motor High-level class to work with motors.
    %
    % This class is supposed to ease the use of the brick's motors. It is possible to set all
    % kinds of parameters (even without connecting to the brick), request the current status of 
    % the motor port and of course send commands to the brick to execute on the respective
    % port.
    %
    % Properties:
    %   Standard
    %       power               - Power level of motor in percent
    %       speedRegulation     - Speed regulation turned on or off
    %       smoothStart         - Degrees/Time for how far/long the motor should smoothly start (depends on limitMode)
    %       smoothStop          - Degrees/Time for how far/long the motor should smoothly stop (depends on limitMode)
    %       limitValue          - Degrees/Time for how far/long the motor should run (depends on limitMode)
    %       brakeMode           - Mode for braking
    %       limitMode           - Mode for motor limit
    %       mode                - Device mode at port, used as tacho mode (format in which tachoCount should be returned)
    %       debug               - Debug mode turned on or off
    %   Dependent
    %       tachoCount          - Current tacho count (either in degrees or rotations)
    %       speed               - Current speed of motor (only valid with speedRegulation turned on)
    %   Get-only
    %       port                - Motor port
    %       motorAtPort         - Is physical motor actually connected to port?
    %       brick               - Brick object from sublayer class Brick -> used as interface to the physical brick.
    %       status              - Connection status at port 
    %       type                - Device type at port
    %
    % Methods:
    %   Standard
    %       Motor               -
    %       connect             - Connects Motor-object to physical brick.
    %       disconnect          - Disconnects Motor-object from physical brick.
    %       update              - Updates Motor-object to current status at its port.
    %       start               - Starts the motor taking all parameters set by user into account. 
    %       stop                - Stops the motor. :)
    %       waitFor             - Stops execution of program as long as motor is running with tacholimit.
    %       isRunning           - Returns whether motor is running (WITH TACHOLIMIT) or not.
    %       reset               - Resets internal tacho count
    %       resetTachoCount     - Resets tacho count to 0 (if running without tacholimit).
    %       togglePolarity      - Switches the direction in which the motor turns.
    %       setProperties       - Sets multiple Motor properties at once using MATLAB's inputParser.
    %       
    %
    %
    % Notes:
    %  * You don't need to create instances of this class. The EV3-class automatically creates
    %    instances for each motor port, and you can work with them via the EV3-object. 
    %    In fact, the connect/disconnect-methods of Motor are even written to be used via 
    %    EV3-class mainly, so you should only work without the EV3-class if you know what you 
    %    are doing!
    %
    % Example
    %  % This small example should only roughly demonstrate how to work with motors.
    %  % Establish connection, set properties on motorA and wait until it's connected. After
    %  % starting, output current speed every 100ms until motor is turned off. Then output
    %  % current tachoCount and disconnect.
    %
    %  b = EV3();
    %  b = EV3('debug', 'on', 'batteryMode', 'Voltage');
    %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
    %  b.motorA.setProperties('Power', 50, 'speedRegulation', 'on', 'limitMode', 'Time');
    %  b.motorA.limitValue = 1000;
    %  b.motorA.brakeMode = 'Coast';
    %  b.motorA.mode = DeviceMode.Motor.Rotations;
    %  while ~b.motorA.motorAtPort  % Wait until physical motor connected to A
    %     b.motorA.update();
    %     pause(0.5);
    %  end
    %  b.motorA.start(); % Motor on portA runs with power 50 for 1000ms and then coasts to a
    %                    % stop
    %  while ~b.motorA.isRunning()
    %     b.motorA.speed
    %     pause(0.1);
    %  end
    %  b.motorA.tachoCount
    %  b.disconnect();
    %  b.delete()
    %
    %
    % Signature
    %  Author: Tim Stadtmann
    %  Date: 2016/05/19
    
    properties  % Standard properties to be set by user
        %power -  Power level of motor in percent
        %   -> Any integer or double in [-100, 100] 
        power;
        
        %speedRegulation  -  Speed regulation turned on or off
        % Turning speed regulation on enables Motor.isRunning(), Motor.waitFor() and correctly
        % reading current speed with Motor.speed.
        %   -> Any valid boolean (0/1/'on'/'off'/'true'/'false')
        speedRegulation;
        
        %smoothStart -  Degrees/Time for how far/long the motor should smoothly start (depends on limitMode)
        %   -> Any integer or double in [0, tachoLimit/2] 
        smoothStart;
        
        %smoothStop -  Degrees/Time for how far/long the motor should smoothly stop (depends on limitMode)
        %   -> Any integer or double in [0, tachoLimit/2] 
        smoothStop;
        
        %limitValue -  Degrees/Time for how far/long the motor should run (depends on limitMode)
        %   -> Any integer or double >= 0 (in ms, if limitMode = 'Time')
        limitValue;  
        
        %brakeMode -  Mode for braking
        %   -> 'Brake' / 'Coast'
        brakeMode;  % Mode for braking: 'Brake' or 'Coast'
        
        %limitMode -  Mode for motor limit
        %   -> 'Tacho' / 'Time'
        limitMode;
        
        %mode -  Device mode at port, used as tacho mode (i.e. what tachoCount should return)
        %   -> DeviceMode.Motor.Degrees / DeviceMode.Motor.Rotations 
        %     (DeviceMode.Motor.Speed is also defined, but this is only used internally.)
        mode;
        
        %debug -  Debug turned on or off
        % In debug mode, everytime a command is passed to the sublayer ('communication layer'),
        % there is feedback in the console about what command has been called, etc.
        %   -> Any valid boolean (0/1/'on'/'off'/'true'/'false')
        debug;
    end
    
    properties (Dependent)  % Parameters to be read directly from physical brick
        tachoCount;  % Current tacho count (either in Degrees or Rotations)
        speed;  % Current speed of motor (only valid with speedRegulation turned on)
    end
    
    properties (SetAccess = 'protected')  % Read-only properties that are set internally or in init-phase      
        %motorAtPort -  Is physical motor actually connected to port?
        % This property will be changed by Motor.update() if changes have happened on the
        % physical brick, depending on status, type and mode values.
        % See also STATUS, TYPE, MODE, UPDATE
        motorAtPort = 0;
        
        %brick -  Brick object from sublayer class Brick -> used as interface to the physical brick.
        % See also BRICK
        brick = 0;
        
        %port -  Motor port
        % This is only the string representation of the motor port to work with.
        % Internally, either MotorPort-, MotorBitfield- or MotorInput-member will be used.
        %   -> 'A' / 'B' / 'C' / 'D'
        port;
        
        %status -  Connection status at port
        % This property will be changed by Motor.update() if changes have happened on the
        % physical brick.
        % See also CONNECTIONTYPE, UPDATE
        status = DeviceMode.Error.Undefined;  
        
        %type -  Device type at port
        % This property will be changed by Motor.update() if changes have happened on the
        % physical brick.
        % See also DEVICEMODE.ERROR, UPDATE
        type = DeviceMode.Error.Undefined; 
    end
    
    properties (Hidden, Access = 'protected')  % Hidden properties for internal use only 
        init = 1;  % Indicates 'init-phase' (Set to 1 as long as constructor is running)
        isConnected = 0;  % Does virtual motor-object have a valid brick-handle?
        
        %% Equivalents for Motor.port and Motor.brakeMode in enumerations
        % Port (obviously) & brakeMode are actual parameters on the brick. To avoid using string
        % comparisons each time they are used, the corresponding values to the given strings
        % are saved (hidden from the user).
        port_;
        brakeMode_; 
        
        %% Indicator for changes which will be needed later on
        changed = 0;  % Saves whether power and/or speed reg have been changed virtually without
                      % updating the physical brick.
        limitSetToZero = 0;  % See motor.start (2nd note) for more info.
        
    end
    
    properties (Hidden, Dependent, Access = 'protected')  % Hidden, dependent properties for internal use only
        portNo;  % Port number
        portInput;  % Port number for input functions
    end
    
    methods  % Standard methods
        %% Constructor
        function motor = Motor(varargin)
            %Motor Sets properties of Motor-object and indicates end of init-phase when it's done.
            %
            % Arguments
            %  * varargin: see EV3::setProperties(ev3, varargin)
            %
            
            motor.setProperties(varargin{:});
            motor.init = 0;
        end
        
        %% Connection
        function connect(motor,brick)
            %connect Connects Motor-object to physical brick.
            %
            % Notes:
            %  * This method actually only saves a handle to a Brick-object which has been
            %    created beforehand (probably by an EV3-object).
            %
            % Arguments
            %  * brick: instance of Brick-class
            %
            % Examples (for use without EV3-class)
            %  m = Motor();
            %  brickInterface = Brick('ioType', 'usb');
            %  m.connect(brickInterface);
            %  % do stuff
            %
            
            if motor.isConnected
                if isBrickValid(motor.brick)
                    error('Motor::connect: Motor-Object already has a brick handle.');
                else
                    warning(['Motor::connect: Motor.isConnected is set to ''True'', but ',...
                             'brick handle is invalid. Deleting invalid handle and ' ,...
                             'resetting Motor.isConnected now...']);
                         
                    motor.brick = 0;
                    motor.isConnected = 0;
                    
                    error('Motor::connect: Aborted connection.');
                end
            end
            
            motor.brick = brick;
            motor.isConnected = 1;
            
            if motor.debug
               fprintf('(DEBUG) Motor-Object connected to brick handle.\n'); 
            end
            
            motor.update();
        end
        
        function disconnect(motor)
            %disconnect Disconnects Motor-object from physical brick.
            %
            % Notes:
            %  * As with Motor::connect, this method actually only sets the property, in which 
            %    the handle to the Brick-class was stored, to 0.
            %
            % Examples (for use without EV3-class)
            %  m = Motor();
            %  brickInterface = Brick('ioType', 'usb');
            %  m.connect(brickInterface);
            %  % do stuff
            %  m.disconnect();
            %  brickInterface.delete(); % Actual disconnecting!!!
            % 
            
            if ~motor.isConnected
                error('Motor::disconnect: No brick connected.');
            end
            
            motor.brick = 0; % Note: actual deleting is done in EV3::disconnect.
            motor.isConnected = 0;
            motor.motorAtPort = 0;
            
            motor.status = DeviceMode.Error.Undefined;
            motor.type = DeviceMode.Error.Undefined;
        end
        
        function update(motor)
            %update Updates motor-object to current status at its port.
            % The parameters type, status and mode, which fully represent any device that can
            % be connected to the brick, will be updated by this method. Furthermore, update()
            % concludes from theses parameters if there is a physical motor currently connected
            % to the port (Motor.motorAtPort).
            %
            % Examples
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  % Connect motor to port A (in 'real life')
            %  b.motorA.power = 50;
            %  b.motorA.start(); -> error, because virtual brick does not know about new motor
            %  b.motorA.type -> outputs DeviceType.Error
            %  b.motorA.update(); 
            %  b.motorA.start(); -> no error
            %  b.motorA.type -> output DeviceType.LargeMotor/DeviceType.MediumMotor
            %  
            
            if ~motor.isConnected
                error(['Motor::update: Motor-Object not connected to brick handle.',...
                    'You have to call motor.connect(brick) first!']);
            end
            
            % Get current status at both ports
            status = motor.getStatus();
            [type, mode] = motor.getTypeMode();
            
            % Conclude if both physical motors are connected
            motor.motorAtPort = 0;
            if uint8(status) >= uint8(ConnectionType.OutputDumb) && ...
                    uint8(status) <= uint8(ConnectionType.OutputTacho)  % Is status of motor valid?
                if (type == DeviceType.MediumMotor || type == DeviceType.LargeMotor) % Is type of motor valid?
                    if strcmp(class(mode), 'DeviceMode.Motor')  % Is mode of motor valid?
                        motor.motorAtPort = 1;
                    end
                end
            end
                
            % Set parameter
            motor.status = status;
            motor.type = type;
            
            if motor.mode ~= mode && strcmp(class(mode), 'DeviceMode.Motor')
                if mode == DeviceMode.Motor.Speed
                    mode = motor.mode;  % This trick prevents set.mode from throwing an error if
                    % the physical motor's mode has internally been set to
                    % DeviceMode.Motor.Speed
                else
                    warning(['Motor::update: Physical motor''s mode was not ',...
                        'the specified one. Changing...']);
                end
            end
            motor.mode = mode;
            
%             oldMode = motor.mode;
%             
%             motor.status = motor.getStatus();
%             [motor.type,newMode] = motor.getTypeMode();
%             
%             motor.motorAtPort = 1;
%             if motor.status~=ConnectionType.OutputTacho || ... % Regular Tacho-Motor connected?
%                 (motor.type~=DeviceType.LargeMotor && motor.type~=DeviceType.MediumMotor) || ...
%                 strcmp(class(motor.mode), 'DeviceMode.Error')
%                 motor.motorAtPort = 0;
%             end
%             
%             
%             if strcmp(class(oldMode),class(newMode)) && oldMode~=newMode
%                 if ~strcmp(class(oldMode), 'DeviceMode.Error') && ...
%                         ~strcmp(class(newMode), 'DeviceMode.Error')
%                     if newMode ~= DeviceMode.Motor.Speed
%                         warning(['Motor::update: Physical motor''s mode was not ',...
%                             'the specified one. Changing...']);
%                     end
%                     
%                     motor.mode = oldMode;
%                 end
%             else 
%                 motor.mode = newMode;
%             end
        end
        
        %% Brick functions
        function start(motor)
            %start Starts the motor taking all parameters set by user into account. 
            %
            % Notes
            %  * Right now, alternatingly calling this function with and without tacho limit
            %    may lead to unexpected behaviour. For example, if you run the motor without
            %    a tacholimit for some time using Coast, then stop using Coast, and then try 
            %    to run the with a tacholimit, it will stop sooner or later than expected, 
            %    or may not even start at all. 
            %  * After calling one of the functions to control the motor with some kind of 
            %    limit (which is done if limit~=0), the physical brick's power/speed value for
            %    starting without a limit (i.e. if limit==0) is reset to zero. So if you want 
            %    to control the motor without a limit after doing so with a limit, you would 
            %    have to set the power manually to the desired value again. (I don't really 
            %    know if this is deliberate or a bug, and at this point, I'm too afraid to ask.)
            %    To avoid confusion, this is done automatically in this special case.
            %    However, this does not even work all the time. If motor does not
            %    start, call stop() and setPower() manually. :/
            % 
            
            if ~motor.isConnected
                error(['Motor::start: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::start: No physical motor connected to Port %s',...
                       motor.port);
            end
            
            if motor.power == 0
               warning('Motor::start: Motor starting with power=0.'); 
            end
            
            if motor.limitValue==0
                if motor.changed || motor.limitSetToZero
                    % See 2nd note
                    motor.stop();
                    motor.setPower(motor.power);
                    motor.limitSetToZero = 0;
                end
                
                motor.brick.outputStart(0, motor.port_);
                
                if motor.debug
                    fprintf('(DEBUG) Motor::start: Called outputStart on Port %s\n', motor.port);
                end
            else
                if motor.isRunning()
                    error('Motor::start: Motor is already running.'); 
                end
                
                if strcmpi(motor.limitMode, 'Tacho')
                    if motor.speedRegulation
                        motor.brick.outputStepSpeed(0, motor.port_, motor.power,...
                            motor.smoothStart, motor.limitValue, motor.smoothStop,...
                            motor.brakeMode_);
                        
                        if motor.debug
                            fprintf('(DEBUG) Motor::start: Called outputStepSpeed on Port %s\n',...
                                    motor.port);
                        end
                    else
                        motor.brick.outputStepPower(0, motor.port_, motor.power,...
                            motor.smoothStart, motor.limitValue, motor.smoothStop,...
                            motor.brakeMode_);
                        
                        if motor.debug
                            fprintf('(DEBUG) Motor::start: Called outputStepPower on Port %s\n',...
                                    motor.port);
                        end
                    end
                elseif strcmpi(motor.limitMode, 'Time')
                    if motor.speedRegulation
                        motor.brick.outputTimeSpeed(0, motor.port_, motor.power,...
                            motor.smoothStart, motor.limitValue, motor.smoothStop,...
                            motor.brakeMode_);
                        
                        if motor.debug
                            fprintf('(DEBUG) Motor::start: Called outputTimeSpeed on Port %s\n',...
                                    motor.port);
                        end
                    else
                        motor.brick.outputTimePower(0, motor.port_, motor.power,...
                            motor.smoothStart, motor.limitValue, motor.smoothStop,...
                            motor.brakeMode_);
                        
                        if motor.debug
                            fprintf('(DEBUG) Motor::start: Called outputTimePower on Port %s\n',...
                                    motor.port);
                        end
                    end
                else
                    % Note: After all the input parsing done in setProperties and set.limitMode
                    %       this REALLY should not happen..
                    error('Motor::start: Limit Mode not valid.');
                end
                
            end
        end
        
        function stop(motor)
            %stop Stops the motor. :)
            
            if ~motor.isConnected
                error(['Motor::stop: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::stop: No physical motor connected to Port %s',...
                       motor.port);
            end
            
            motor.brick.outputStop(0, motor.port_, motor.brakeMode_);
            
            if motor.debug
                fprintf('(DEBUG) Motor::stop: Called outputStop on Port %s\n', motor.port);
            end
        end
        
        function waitFor(motor)
            %waitFor Stops execution of program as long as motor is running (WITH TACHOLIMIT).
            %
            % Notes:
            %  * This one's a bit tricky. The opCode OutputReady makes the brick stop sending
            %    responses until the motor has stopped. For security reasons, in this toolbox 
            %    there is an internal timeout for receiving messages from the brick. It raises
            %    an error if a reply takes too long, which would happen in this case. To
            %    workaround, there is an infinite loop that catches errors from outputReady and
            %    continues then, until outputReady will actually finish without an error.
            %  * OutputReady (like OutputTest in isRunning) sometimes doesn't work. If 
            %    outputReady returns in less than a second, another while-loop iterates until 
            %    the motor has stopped, this time using motor.isRunning() (this only works as 
            %    long as not both OutputTest and OutputReady are buggy).
            %
            
            if ~motor.isConnected
                error(['Motor::waitFor: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::waitFor: No physical motor connected to Port %s',...
                       motor.port);
            elseif ~motor.limitValue
                error(['Motor::waitFor: Motor has no tacho limit. ' ,...
                         'Can''t reliably determine whether it is running or not.']);
            end
            
            tic;
            while 1
                try
                    warning('off','all');
                    
                    motor.brick.outputReady(0, motor.port_);
                    t = toc;
                    
                    if t < 1
                        while motor.isRunning()  % If outputReady correctly returned in less 
                                                 % than a second, isRunning should instantly send 0.
                        end
                    end
                    
                    warning('on','all');
                    break;
                catch  % TO DO: Catch only timeout exception, otherwise death and destruction possible (aka infinite loop)
                    continue;
                end
            end
            
            if motor.debug
                fprintf('(DEBUG) Motor::waitFor: Called outputReady on Port %s\n', motor.port);
            end
        end
        
        function running = isRunning(motor)
            %isRunning Returns whether motor is running (WITH TACHOLIMIT) or not.
            %
            % Notes:
            %  * This *mostly* works. Sometimes this falsely returns 0 if isRunning() gets 
            %    called immediately after starting the motor.
            %
            
            if ~motor.isConnected
                error(['Motor::isRunning: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::isRunning: No physical motor connected to Port %s',...
                       motor.port);
            elseif ~motor.limitValue
                warning(['Motor::isRunning: Motor has no tacho limit. ' ,...
                         'Can''t reliably determine whether it is running or not.']);
            end
            
            running = motor.brick.outputTest(0, motor.port_);
            
            if motor.debug
                fprintf('(DEBUG) Motor::isRunning: Called outputReady on Port %s\n', motor.port);
            end
        end
		
        function reset(motor)
            %reset Resets internal tacho count
            % The internal tacho count is used for positioning the motor. For example, when the
            % motor is running with a tacho limit, internally it uses another counter than the
            % one read by tachoCount. This internal tacho count needs to be reset if you 
            % physically change the motor's position.
            % See also MOTOR.RESETTACHOCOUNT
            
            if ~motor.isConnected
                error(['Motor::reset: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::reset: No physical motor connected to Port %s',...
                       motor.port);
            end
            
            motor.brick.outputReset(0, motor.port_);
            
            if motor.debug
                fprintf('(DEBUG) Motor::reset: Called outputReset on Port %s\n',...
                          motor.port);
            end
        end
        
        function resetTachoCount(motor)
            %resetTachoCount Resets tacho count to 0 if running without tacholimit
            % Compared to motor.reset, this resets the 'sensor mode' tacho count, a second 
            % tacho counter. This counter is used for reading the tacho count with inputRead 
            % and outputGetCount (via motor.tachoCount).
            % See also MOTOR.RESET
            
            if ~motor.isConnected
                error(['Motor::resetTachoCount: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::resetTachoCount: No physical motor connected to Port %s',...
                       motor.port);
            end
            
            motor.brick.outputClrCount(0, motor.port_);
            if motor.debug
                fprintf('(DEBUG) Motor::resetTachoCount: Called outputClrCount on Port %s\n',...
                          motor.port);
            end
        end
        
        function togglePolarity(motor)
            %togglePolarity Switches the direction in which the motor turns.
            if ~motor.isConnected
                error(['Motor::togglePolarity: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::togglePolarity: No physical motor connected to Port %s',...
                       motor.port);
            end
            
            motor.brick.outputPolarity(0, motor.port_, 0);
            if motor.debug
                fprintf('(DEBUG) Motor::togglePolarity: Called outputPolarity on Port %s\n', motor.port);
            end
        end
        
        %% Setter
        function set.port(motor, port)
            if ~isPortValid(class(motor),port)
                error('Motor::set.port: Given port is not a valid port.');
            end
            
            if ischar(port)
                motor.port = port;
                [motor.port_, ~, ~] = str2PortParam(class(motor), port);
            else
                error('Motor::set.port: Port has to be a string.');
            end
        end
        
        function set.power(motor, power)
            if ~isnumeric(power)
                error('Motor::set.power: Given parameter is not a numeric.');
            elseif power<-100 || power>100
                warning('Motor::set.power: Motor power has to be an element of [-100,100]!');
                error('Motor::set.power: Given motor power is out of bounds.');
            elseif power == 0
                if ~motor.init
                    warning('Motor::set.power: Setting power to zero...');
                end
            elseif power == motor.power
                warning('Motor::set.power: Motor power already is %d',power);
            end
            
            motor.power = power;  % Set power parameter.
            motor.changed = 1;  % Indicate that virtual and physical brick have different power
                                % parameters right now. See 'setPower' for more info.
            motor.setPower(power); % Update physical brick's power parameter.
        end
        
        function set.mode(motor, mode)
            % Convert given mode to DeviceMode if necessary
            if strcmp(class(mode), 'uint8') || strcmp(class(mode), 'double')
                mode = DeviceMode(motor.type, uint8(mode));
            end
            
            if isModeValid(mode, DeviceType.LargeMotor)
                if mode == DeviceMode.Motor.Speed
                    error(['Motor::update: Physical motor''s mode has to be either ' ,...
                        'DeviceMode.Motor.Degrees or DeviceMode.Motor.Rotations. ' ,...
                        '(DeviceMode.Motor.Speed is for internal use only.']);
                end
                
                % At this point, mode is a valid DeviceMode for Motor-objects.
                
                % If motor is currently not connected, allow changes
                if strcmp(class(mode),'DeviceMode.Error') && ~motor.motorAtPort
                    motor.mode = mode;
                    return;
                end
                motor.mode = mode;
                
                if motor.isConnected && motor.motorAtPort
                    motor.setMode(mode);
                end
            else
                error(['Motor::update: Physical motor''s mode has to be either ' ,...
                    'DeviceMode.Motor.Degrees or DeviceMode.Motor.Rotations.']);
            end
        end
        
        function set.speedRegulation(motor, speedRegulation)
            if ~isBool(speedRegulation)
                error('Motor::set.speedRegulation: Given parameter is not a bool.');
            end
            
            if ischar(speedRegulation)
                motor.speedRegulation = str2bool(speedRegulation);
            else
                motor.speedRegulation = speedRegulation;
            end
            motor.changed = 1;
        end
        
        function set.smoothStart(motor, steps)
            if ~isnumeric(steps)
                error('Motor::set.smoothStart: Given parameter is not a numeric.');
            elseif steps<0
                warning('Motor::set.smoothStart: Smooth start steps have to be positive.');
                error('Motor::set.smoothStart: Smooth start steps are out of bounds.');
            elseif isempty(motor.limitValue)
                warning(['Motor::set.smoothStart: Smooth start steps should be set after ',...
                    'setting motor.limitValue. Setting smooth start steps to zero...']);
                steps = 0;
            elseif steps>motor.limitValue
                error(['Motor::set.smoothStart: Smooth start steps are greater than ',...
                    'actual limitValue.']);
            end
            
            motor.smoothStart = steps;
        end
        
        function set.smoothStop(motor, steps)
            if ~isnumeric(steps)
                error('Motor::set.smoothStop: Given parameter is not a numeric.');
            elseif steps<0
                warning('Motor::set.smoothStop: Smooth stop steps have to be positive.');
                error('Motor::set.smoothStop: Smooth stop steps are out of bounds.');
            elseif isempty(motor.limitValue)
                warning(['Motor::set.smoothStop: Smooth stop steps should be set after ',...
                    'setting motor.limitValue. Setting smooth stop steps to zero...']);
                steps = 0;
            elseif steps>motor.limitValue
                error(['Motor::set.smoothStop: Smooth stop steps (%d) are greater than ',...
                    'actual limitValue (%d).'], steps, motor.limitValue);
            end
            
            motor.smoothStop = steps;
        end
        
        function set.debug(motor, debug)
            if ~isBool(debug)
                error('Motor::set.debug: Given parameter is not a bool.');
            end
            
            if ischar(debug)
                motor.debug = str2bool(debug);
            else
                motor.debug = debug;
            end
        end
    	
        function set.brakeMode(motor, brakeMode)
            validModes = {'Coast', 'Brake'};
            if ~ischar(brakeMode) || ~any(validatestring(brakeMode, validModes))
                error('Motor::set.brakeMode: Given parameter is not a valid brake mode.');
            else 
                motor.brakeMode = brakeMode;
                motor.brakeMode_ = str2brake(brakeMode); 
            end
        end
        
        function set.limitMode(motor, limitMode)
            validModes = {'Time', 'Tacho'};
            if ~ischar(limitMode) || ~any(validatestring(limitMode, validModes))
                error('Motor::set.limitMode: Given parameter is not a valid limit mode.');
            else 
                motor.limitMode = limitMode;
            end
        end
        
        function set.limitValue(motor, limitValue)
            if ~isnumeric(limitValue)
                error('Motor::set.limitValue: Given parameter is not a numeric.');
            elseif limitValue<0
                warning('Motor::set.limitValue: limitValue has to be positive!');
                error('Motor::set.limitValue: Given limitValue is out of bounds.');
            elseif any(motor.limitValue) 
                if motor.limitValue~=0 && limitValue==0
                    motor.limitSetToZero = 1;
                end
            end
            motor.limitValue = limitValue;
        end
        
        function set.brick(motor, brick)
            if ~isBrickValid(brick)
                error('Motor::set.brick: Handle to brick not valid.');
            else
                motor.brick = brick;
            end
        end
        
        function setProperties(motor, varargin)
            %setProperties Sets multiple Motor properties at once using MATLAB's inputParser.
            %
            % Arguments
            %  * 'debug', 0/1/'on'/'off'/'true'/'false'
            %  * 'smoothStart', integer or double in [0, tachoLimit/2] 
            %  * 'smoothStop', integer or double in [0, tachoLimit/2] 
            %  * 'speedRegulation', 0/1/'on'/'off'/'true'/'false'
            %  * 'brakeMode', 'Coast'/'Brake'
            %  * 'limitMode', 'Time'/'Tacho'
            %  * 'limit', integer or double > 0
            %  * 'power', integer or double in [-100,100]
            %  * 'mode', DeviceMode.Motor.Degrees/DeviceMode.Motor.Rotations
            %  * 'batteryMode', 'Voltage'/'Percentage'
            %
            % Example
            %  b = EV3(); 
            %  b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');
            %  b.motorA.setProperties('debug', 'on', 'power', 50, 'limit', 720, 'speedRegulation', 'on');
            %  % Instead of: b.motorA.debug = 'on'; 
            %  %             b.motorA.power = 50;
            %  %             b.motorA.limit = 720;
            %  %             b.motorA.speedRegulation = 'on';
            %
            
            p = inputParser();
            p.KeepUnmatched = 1;  % For possible use in SyncMotor::setProperties
            
            % Set default values
            if motor.init
                defaultSmoothStart = 0;
                defaultSmoothStop = 0;
                defaultDebug = 0;
                defaultSpeedReg = 0;
                defaultBrakeMode = 'Coast';
                defaultLimitMode = 'Tacho';
                defaultLimit = 0;
                defaultPower = 0;
                defaultMode = DeviceMode.Motor.Degrees;
            else
                defaultSmoothStart = motor.smoothStart;
                defaultSmoothStop = motor.smoothStop;
                defaultDebug = motor.debug;
                defaultSpeedReg = motor.speedRegulation;
                defaultBrakeMode = motor.brakeMode;
                defaultLimitMode = motor.limitMode;
                defaultLimit = motor.limitValue;
                defaultPower = motor.power;
                defaultMode = motor.mode;
            end
            
            % set valid values and save them in a cell array
%             validBools = {num2str(0), num2str(1), 'off', 'on'};
%             validBrakeModes = {num2str(BrakeMode.Coast), num2str(BrakeMode.Brake), 'Coast', 'Brake'};
%             validPorts = {num2str(MotorBitfield.MotorA), num2str(MotorBitfield.MotorB),...
%                           num2str(MotorBitfield.MotorC), num2str(MotorBitfield.MotorD),...
%                           num2str(Device.MotorSync),...
%                           'A', 'B', 'C', 'D'};
%             validLimitModes = {'Time', 'Tacho'};
            
            % define anonymous functions that will return whether given value in varargin is 
            % valid
%             checkBools = @(x) any(validatestring(num2str(x),validBools));
%             checkBrakeMode = @(x) any(validatestring(num2str(x),validBrakeModes));
%             checkLimitMode = @(x) any(validatestring(x, validLimitModes));
%             checkPorts = @(x) any(validatestring(num2str(x),validPorts));
%             checkLimit = @(x) x>=0;
%             checkPower = @(x) isnumeric(x) && x>=-100 && x<=100;
            
            % Add parameter
            if motor.init
                p.addRequired('port');%, checkPorts);
            end
            p.addOptional('smoothStart', defaultSmoothStart);%, checkBools);
            p.addOptional('smoothStop', defaultSmoothStop);%, checkBools);
            p.addOptional('debug', defaultDebug);%, checkBools);
            p.addOptional('speedRegulation', defaultSpeedReg);%, checkBools);
            p.addOptional('brakeMode', defaultBrakeMode);%, checkBrakeMode);
            p.addOptional('limitMode', defaultLimitMode);%, checkLimitMode);
            p.addOptional('limitValue', defaultLimit);%, checkLimit);
            p.addOptional('power', defaultPower);%, checkPower);
            p.addOptional('mode', defaultMode);
            
            % Parse...
            p.parse(varargin{:});
            
            % Set properties
            if motor.init
               motor.port = p.Results.port; 
            end
            motor.limitValue = p.Results.limitValue;
            motor.limitMode = p.Results.limitMode;
%             if ~motor.init && motor.power ~= p.Results.power  % To avoid unnecessary communication with physical brick
%                 motor.power = p.Results.power;
%             end
            motor.power = p.Results.power;
            motor.brakeMode = p.Results.brakeMode;
            motor.smoothStart = p.Results.smoothStart;
            motor.smoothStop = p.Results.smoothStop;
            motor.debug = p.Results.debug;
            motor.speedRegulation = p.Results.speedRegulation;
            motor.mode = p.Results.mode;
        end
        
        %% Getter
        function portNo = get.portNo(motor)
            portNo = motor.getPortNo();
        end
        
        function portInput = get.portInput(motor)
            portInput = motor.getPortInput();
        end
        
        function cnt = get.tachoCount(motor)
            if ~motor.isConnected || ~motor.motorAtPort
                warning('Motor::get.tachoCount: Could not detect motor at port %s.', ...
                         motor.port);
                
                cnt = 0;
                return;
            end
            
            cnt = motor.getTachoCount();
        end
        
        function speed = get.speed(motor)
            if ~motor.isConnected || ~motor.motorAtPort
                warning('Motor::get.tachoCount: Could not detect motor at port %s.', ...
                         motor.port);
                
                speed = 0;
                return;
            end
            
            speed = motor.getSpeed();
        end
        
        %% Display
        function display(motor)
            warning('off','all');
            builtin('disp', motor);
            warning('on','all');
        end
    end
    
    methods (Access = 'protected')  % Private brick functions that are wrapped by dependent params
        function setPower(motor, power)
            %setPower Sets given power value on the physical brick.
            %
            % Notes:
            %  * If motor is running with a limit, calling outputSpeed/outputPower, to
            %    manually set the power on the physical brick, would stop the motor and adopt
            %    the new power value on next start. To avoid this, the motor could be 'restarted'
            %    with the new value instantly. However, this sometimes leads to unexpected behaviour.
            %    Therefore, if motor is running with a limit, setPower aborts with a warning.
            %
            
            if ~motor.isConnected || ~motor.motorAtPort
                return;
            end
            
%             if ~isnumeric(power)
%                 error('Motor::setPower: Given parameter is not a numeric.');
%             elseif power<-100 || power>100
%                 warning('Motor::setPower: Motor power has to be an element of [-100,100]!');
%                 error('Motor::setPower: Given motor power is out of bounds.');
%             end
            
            if motor.limitValue~=0 && motor.isRunning()
                % motor.start();
                
                warning(['Motor::setPower: Can''t set power if motor is running with a ',...
                         'tacholimit. This would mess up the internal tacho state.']);
                return;
            else
                if motor.speedRegulation
                    motor.brick.outputSpeed(0, motor.port_, power);
                    
                    if motor.debug
                        fprintf('(DEBUG) Motor::setPower: Called outputSpeed on Port %s\n', motor.port);
                    end
                else
                    motor.brick.outputPower(0, motor.port_, power);
                    
                    if motor.debug
                        fprintf('(DEBUG) Motor::setPower: Called outputPower on Port %s\n', motor.port);
                    end
                end
            end
            motor.changed = 0;
        end 
        
        function setMode(motor, mode)
            if ~motor.isConnected || ~motor.motorAtPort
                return;
            end
            
            motor.brick.inputReadSI(0, motor.portInput, mode);  % Reading a value implicitly 
                                                                   % sets the mode.

            if motor.debug
                fprintf('(DEBUG) Motor::setMode: Called inputReadSI on input port %d\n',...
                         uint8(motor.portInput));
            end        
        end
        
        function [type,mode] = getTypeMode(motor)
            if ~motor.isConnected
                error(['Motor::getTypeMode: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            end
            
            [typeNo,modeNo] = motor.brick.inputDeviceGetTypeMode(0, motor.portInput);
            type = DeviceType(typeNo);
            mode = DeviceMode(type,modeNo);
            
            if motor.debug
                fprintf('(DEBUG) Motor::getTypeMode: Called inputDeviceGetTypeMode on Port %s\n',...
                         motor.port);
            end
        end
        
        function status = getStatus(motor)
            if ~motor.isConnected
                error(['Motor::getStatus: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            end
            
            statusNo = motor.brick.inputDeviceGetConnection(0, motor.portInput);
            status = ConnectionType(statusNo);
            
            if motor.debug
                fprintf('(DEBUG) Motor::getStatus: Called inputDeviceGetConnection on Port %s\n',...
                         motor.port);
            end
        end
        
        function cnt = getTachoCount(motor)
            if ~motor.isConnected
                error(['Motor::getTachoCount: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::getTachoCount: No physical motor connected to Port %s',...
                       motor.port);
            end
            
            if motor.mode == DeviceMode.Motor.Degrees
                cnt = motor.brick.outputGetCount(0, motor.portNo);
                if motor.debug
                    fprintf('(DEBUG) Motor::getTachoCount: Called outputGetCount on Port %s\n', motor.port);
                end
            elseif motor.mode == DeviceMode.Motor.Rotations
                cnt = motor.brick.inputReadSI(0, motor.portInput, motor.mode);
                if motor.debug
                    fprintf('(DEBUG) Motor::getTachoCount: Called inputReadSI on Port %s\n', motor.port);
                end
            else
                error('Motor::getTachoCount: Motor mode not valid!');
            end
        end
        
        function speed = getSpeed(motor)
            if ~motor.isConnected
                error(['Motor::getSpeed: Motor-Object not connected to brick handle.',...
                       'You have to call motor.connect(brick) first!']);
            elseif ~motor.motorAtPort
                error('Motor::getSpeed: No physical motor connected to Port %s',...
                       motor.port);
            end
            
            if ~motor.speedRegulation
                warning('Motor::getSpeed: Speed only valid with speed regulation turned on.');
            end
            speed = motor.brick.inputReadSI(0, motor.portInput, DeviceMode.Motor.Speed);
            
            if motor.debug
                fprintf('(DEBUG) Motor::getSpeed: Called inputReadSI on Port %s\n', motor.port);
            end
        end
    end
    
    methods (Hidden, Access = 'protected')  % Wrapper for utility functions, hidden from user
        % These are overridden in SyncMotor so the 'right' utility function
        % will always be called.
        function portNo = getPortNo(motor)
            if isempty(motor.port_)
                error(['Motor::portNo: This method should only be called AFTER ',...
                    'setting motor.port.']);
            end
            
            portNo = bitfield2port(motor.port_);
        end
        
        function portInput = getPortInput(motor)
            if isempty(motor.port_)
               error(['Motor::portInput: This method should only be called AFTER ',... 
                      'setting motor.port.']);
            end
            
            portInput = port2input(motor.portNo);
        end
    end
end
