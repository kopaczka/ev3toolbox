function isValid = isModeValid(mode, type)
% Returns whether given mode is a valid mode in given type.
    isValid = 1;
    
    if strcmp(class(mode), 'DeviceMode.Error')
        return;
    end

    switch type
        case DeviceType.NXTTouch
            if ~strcmp(class(mode), 'DeviceMode.NXTTouch')
                isValid = 0;
            end
        case DeviceType.NXTLight
            if ~strcmp(class(mode), 'DeviceMode.NXTLight')
                isValid = 0;
            end
        case DeviceType.NXTSound
            if ~strcmp(class(mode), 'DeviceMode.NXTSound')
                isValid = 0;
            end
        case DeviceType.NXTColor
            if ~strcmp(class(mode), 'DeviceMode.NXTColor')
                isValid = 0;
            end
        case DeviceType.NXTUltraSonic
            if ~strcmp(class(mode), 'DeviceMode.NXTUltraSonic')
                isValid = 0;
            end
        case DeviceType.NXTTemperature
            if ~strcmp(class(mode), 'DeviceMode.NXTTemperature')
                isValid = 0;
            end
        case DeviceType.LargeMotor
            if ~strcmp(class(mode), 'DeviceMode.Motor')
                isValid = 0;
            end
        case DeviceType.MediumMotor
            if ~strcmp(class(mode), 'DeviceMode.Motor')
                isValid = 0;
            end
        case DeviceType.Touch
            if ~strcmp(class(mode), 'DeviceMode.Touch')
                isValid = 0;
            end
        case DeviceType.Color
            if ~strcmp(class(mode), 'DeviceMode.Color')
                isValid = 0;
            end
        case DeviceType.UltraSonic
            if ~strcmp(class(mode), 'DeviceMode.UltraSonic')
                isValid = 0;
            end
        case DeviceType.Gyro
            if ~strcmp(class(mode), 'DeviceMode.Gyro')
                isValid = 0;
            end
        case DeviceType.InfraRed
            if ~strcmp(class(mode), 'DeviceMode.InfraRed')
                isValid = 0;
            end
    end
end

