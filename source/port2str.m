function port = port2str(device, inp)
% Converts a port-enum to corresponding-string.
    if ~ischar(device)
        error(['port2str: First argument has to be a string (''Sensor'', ',...
            '''Motor'' or ''SyncMotor'')']); 
    end
    
    if strcmpi(device, 'Motor')
        if ~ischar(inp)
            if inp == MotorBitfield.MotorA || inp == MotorPort.MotorA || ...
                    inp == MotorInput.MotorA
                port = 'A';
            elseif inp == MotorBitfield.MotorB || inp == MotorPort.MotorB || ...
                    inp == MotorInput.MotorB
                port = 'B';
            elseif inp == MotorBitfield.MotorC || inp == MotorPort.MotorC || ...
                    inp == MotorInput.MotorC
                port = 'C';
            elseif inp == MotorBitfield.MotorD || inp == MotorPort.MotorD || ...
                    inp == MotorInput.MotorD
                port = 'D';            
            else
                error('port2str: Given parameter is not a valid motor port.' );
            end
        else
            warning('port2str: Given parameter already is a string.');
            port = inp;
        end
    elseif strcmpi(device, 'Sensor')
        if ~ischar(inp)
            if inp == SensorPort.Sensor1
                port = '1';
            elseif inp == SensorPort.Sensor2
                port = '2';
            elseif inp == SensorPort.Sensor3
                port = '3';
            elseif inp == SensorPort.Sensor4
                port = '4';            
            else
                error('port2str: Given parameter is not a valid sensor port.' );
            end
        else
            warning('port2str: Given parameter already is a string.');
            port = inp;
        end
    elseif strcmpi(device, 'SyncMotor')
        if ~ischar(inp)
            switch inp
                case MotorBitfield.MotorA+MotorBitfield.MotorB
                    port = 'AB';
                case MotorBitfield.MotorA+MotorBitfield.MotorC
                    port = 'AC';
                case MotorBitfield.MotorA+MotorBitfield.MotorD
                    port = 'AD';
                case MotorBitfield.MotorB+MotorBitfield.MotorC
                    port = 'BC';
                case MotorBitfield.MotorB+MotorBitfield.MotorD
                    port = 'BD';
                case MotorBitfield.MotorC+MotorBitfield.MotorD
                    port = 'CD';
                otherwise
                    error('port2str: Given parameter is not a valid motor port.' );
            end
        else
            warning('port2str: Given parameter already is a string.');
            port = inp;
        end
    else
        error(['port2str: First argument has to be either ''Sensor'', ',...
               '''Motor'' or ''SyncMotor''']);
    end
end