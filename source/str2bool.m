function bool = str2bool(inp)
% Converts a boolean-string to a numeric 0/1.

    bool = 0;
    if ischar(inp)
        if strcmpi(inp, 'on') || strcmpi(inp, 'true')
            bool = 1;
        elseif ~strcmpi(inp, 'off') && ~strcmpi(inp, 'false')
            error('str2bool: Given parameter is not a valid string.');
        end
    elseif isnumeric(inp)
        % warning('str2bool: Given parameter already is a numeric. Returning...');
        if bool==0 || bool==1
            bool = inp;
        end
    end
end
