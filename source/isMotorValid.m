function isValid = isMotorValid(motor)
% Returns whether given motor object is valid or not.
    isValid = 0;
    if ~isempty(motor)
        if isa(motor, 'Motor') && motor.isvalid
            isValid = 1;
        end
    end
end
