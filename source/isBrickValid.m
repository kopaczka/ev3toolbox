function isValid = isBrickValid(testBrick)
% Returns whether given brick object is valid or not.
    isValid = 0;
    if ~isempty(testBrick)
        % The second case (after the '||') is allowed as a default value (think of it as a nullptr).
        if (isa(testBrick, 'Brick') && testBrick.isvalid) || ...
           (isnumeric(testBrick) && testBrick==0)
            isValid = 1;
        end
    end
end

