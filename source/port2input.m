function input = port2input(port)
% Converts a motorPort-enum to motorInput-enum.
    input = MotorInput(port+16);
end