function isValid = isBool(inp)
% Returns whether given boolean is valid or not.
    isValid = 0;
    if ischar(inp)
        if strcmpi(inp, 'on') || strcmpi(inp, 'true') ...
            || strcmpi(inp, 'off') || strcmpi(inp, 'false')
            isValid = 1;
        end
    elseif isnumeric(inp)
        if inp==1 || inp==0
            isValid = 1;
        end
    end
end

