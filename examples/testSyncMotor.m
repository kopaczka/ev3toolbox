% Connect with physical brick. If battery charge less than 10% abort. Otherwise,
% create a SyncMotor-object for ports A & B and set some propeties.
% Turn speed regulation on motors A & B on in order to be able to read speed values later.
% ('help Motor.speed' for a little more info)
% Pause program until both motors are connected and then start them.
% As long as they run, print out current speed of both motors.
% Finally, disconnect.

clear all;

b = EV3();
b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm1');

if b.batteryValue < 10
    fprintf('Battery charge too low.');
    b.disconnect();
else
    s = b.coupleMotors('AB', 'useMotorParams', 0);
    s.setProperties('Power', 50, 'speedRegulation', 'on', ...
                      'limitMode', 'Time', 'turnRatio', 50);
    s.limitValue = 5000;
    s.brakeMode = 'Coast';
    b.motorA.speedRegulation = 'on';
    b.motorB.speedRegulation = 'on';
    s.mode = DeviceMode.Motor.Rotations;
    while ~s.motorAtPort
        s.update();
        pause(0.5);
    end
    s.start();
    while s.isRunning()
        fprintf('Speed on A: %d\n',b.motorA.speed);
        fprintf('Speed on B: %d\n',b.motorB.speed);
        pause(0.1);
    end
    s.disconnect(); s.delete();
    b.disconnect(); b.delete();
end
