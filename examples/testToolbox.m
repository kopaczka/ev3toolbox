b = EV3('debug', 1);
b.connect('ioType', 'bt', 'serPort', '/dev/rfcomm0');

tic;
t = 0;
while 1
    if b.motorA.motorAtPort && b.motorB.motorAtPort 
        if b.sensor3.sensorAtPort && b.sensor4.sensorAtPort
             if b.sensor3.type == DeviceType.Color
                 b.sensor3.value
                 break;
             end
        end
    end
    b.update();
    t = [t; toc]; 
end

b.motorA.setProperties('power', 50, 'speedRegulation', 'on', 'smoothStart', 10, 'limitMode', 'time', 'limitValue', 3000);
b.motorA.start();

while b.motorA.isRunning()
    b.sensor3.value
    b.sensor4.value
    b.motorA.tachoCount
end

b.motorB.power = 50;
b.motorB.limitValue = 4*360;
b.motorB.waitFor();

b.sensor3.value
b.motorA.tachoCount
b.sensor3.reset
b.motorA.tachoCount

b.beep

b.disconnect
b.delete
